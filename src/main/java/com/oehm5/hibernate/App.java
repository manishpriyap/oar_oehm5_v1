package com.oehm5.hibernate;

import com.oehm5.hibernate.dao.RegisterDao;
import com.oehm5.hibernate.entity.UserRegistration;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        UserRegistration registration = new UserRegistration();
        registration.setId(123L);
        registration.setUserName("satya@123");
        registration.setPassword("passwroed");
        registration.setEmail("satya@gmail.com");
        registration.setMobileNumber("1234567890");
        
        RegisterDao registerDao = new RegisterDao();
        registerDao.saveUser(registration);
    }
}
